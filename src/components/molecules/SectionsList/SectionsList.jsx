import React from 'react';
import { Stack } from '@mui/material';
import { Article, Call, Code, House, People } from '@mui/icons-material';

import { NavbarButton } from '../../atoms';

const SectionsList = () => {
    return (
        <Stack direction={{sm: "column", md: "row"}} gap={2} justifyContent="space-around" width={{sm: "100%", lg: "70%"}}>
            <NavbarButton icon={<House />} title="Home" />
            <NavbarButton icon={<People />} title="About Me" />
            <NavbarButton icon={<Article />} title="My Resume" />
            <NavbarButton icon={<Code />} title="My Projects" />
            <NavbarButton icon={<Call />} title="Contact Me" />
        </Stack>
    )
}

export default SectionsList;
