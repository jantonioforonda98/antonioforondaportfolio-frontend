import { Box, Card, Stack, Typography } from "@mui/material";

import style from './DescriptionCard.module.css';

const DescriptionCard = ({ title, text, image, reverse }) => {
    return (
        <Card className={style.card}>
            <Stack direction={reverse ? {sm: "column-reverse", md: "row-reverse"} : {sm: "column", md: "row"}} justifyContent="space-around">
                <Stack direction="column">
                    <Typography className={style.title}>{title}</Typography>
                    <Box className={style.box}>
                        {text}
                    </Box>
                </Stack>
                <img src={image} alt={title}></img>
            </Stack>
        </Card>
    )
}

export default DescriptionCard;
