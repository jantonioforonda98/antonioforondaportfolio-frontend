import { Box, Typography } from "@mui/material";

import { DescriptionCard } from "../../molecules";
import avatar from '../../../Assets/spAvatar.png';
import style from './AboutMe.module.css';

const AboutMe = () => {
    return (
        <Box className={style.box}>
            <DescriptionCard
                image={avatar}
                text={
                    <Typography>
                        Antonio is a highly motivated and organized professional with a wealth of experience in project management and marketing. He is passionate about helping businesses achieve their goals and make a positive impact in their communities.
                        Antonio holds a bachelor's degree in Business Administration from the University of Chicago, and has a decade of experience in the field. He has worked for companies such as KPMG and Deloitte, managing teams and developing marketing strategies. He has developed a deep understanding of customer relations, and his strong analytical skills enable him to come up with innovative solutions for any given situation.
                        In his spare time, Antonio loves to travel and explore different cultures. He also enjoys reading, writing, and spending time with his family. He is a firm believer in the importance of giving back, and he participates in a number of volunteer activities in his local community.
                        Antonio is always looking to take on new challenges and expand his skillset. He is confident in his ability to deliver successful projects, and he is always ready to take on new opportunities. If you are looking for an experienced and dedicated professional, Antonio is the right choice for you.
                    </Typography>
                }
                title="About Antonio"
            />
        </Box>
    );
}

export default AboutMe;