import { Box, Typography } from '@mui/material';

import django from '../../../Assets/django.jpg';
import csharp from '../../../Assets/c#.jpg';
import react from '../../../Assets/react.jpg';
import { DescriptionCard } from '../../molecules/index';
import style from './MyProjects.module.css';

const MyProjects = () => {
    return(
        <Box className={style.box}>
            <DescriptionCard
                image={django}
                text={
                    <>
                        <Typography>An e-commerce website where you can browse through different products, make purchases, and open an account</Typography>
                        <Typography>Link: </Typography>
                        <a href='https://antonioforonda.pythonanywhere.com/'>Here</a>
                    </>
                }
                title="E-Commerce"
            />
            <DescriptionCard
                image={react}
                text={
                    <>
                        <Typography>A dental clinic webpage developed with react, and deployed with Azure.</Typography>
                        <Typography>Link: </Typography>
                        <a href='https://clinica-dental-saint-augustine.azurewebsites.net/'>Here</a>
                    </>
                    }
                title="Clinica Saint Augustine"
                reverse
            />
            <DescriptionCard
                image={react}
                text={
                    <>
                        <Typography>This site was developed with react including libraries like redux, and components, and icons from material ui</Typography>
                    </>
                    }
                title="Portfolio"
            />
            <DescriptionCard
                image={csharp}
                text={
                    <Typography>In the past I collaborated in projects like Applican Tracker or Meetpoint for Jala group using the C#/React stack along with relationl dbs and no relational ones</Typography>
                }
                title="Other Projects"
                reverse
            />
        </Box>
    )
}

export default MyProjects;