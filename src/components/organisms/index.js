import AboutMe from './AboutMe/AboutMe';
import Contact from './Contact/Contact';
import Home from './Home/Home';
import Navbar from './Navbar/Navbar';
import MyProjects from './MyProjects/MyProjects';
import MyResume from './MyResume/MyResume';

export {AboutMe, Contact, Home, Navbar, MyProjects, MyResume };
