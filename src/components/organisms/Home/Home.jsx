import { Box, Typography } from '@mui/material';

import avatar from '../../../Assets/spAvatar.png';
import commigSoon from '../../../Assets/commingSoon.gif';
import { DescriptionCard } from '../../molecules/index';
import style from './Home.module.css';

const Home = () => {
    return(
        <Box className={style.box}>
            <DescriptionCard
                image={avatar}
                text={
                    <>
                        <Typography className={style.text} sx={{display: "inline"}}>My name is </Typography>
                        <Typography className={style.name}>Antonio Foronda,</Typography>
                        <Typography className={style.text}>and I am a full stack developer</Typography>
                    </>
                }
                title="Welcome!"
            />
            <DescriptionCard
                image={commigSoon}
                text={
                    <>
                        <Typography>Please reach out. The site is being continiously delployed, and I am always looking for improvement</Typography>
                    </>
                    }
                title="Have any suggestions?"
                reverse
            />
        </Box>
    )
}

export default Home;
