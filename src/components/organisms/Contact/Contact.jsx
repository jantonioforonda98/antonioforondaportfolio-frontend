import { Box, Typography } from '@mui/material';

import commigSoon from '../../../Assets/commingSoon.gif';
import { DescriptionCard } from '../../molecules/index';
import style from './Contact.module.css';

const Contact = () => {
    return(
        <Box className={style.box}>
            <DescriptionCard
                image={commigSoon}
                text={
                    <>
                        <Typography>Sorry, the site is not ready yet, Please come back soon</Typography>
                        <Typography>Note: The site is being continiously delployed, so it may fail during deployment</Typography>
                    </>
                    }
                title="Sorry!"
                reverse
            />
        </Box>
    )
}

export default Contact;