import React from "react";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Stack,
  Toolbar,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { Menu } from '@mui/icons-material';

import logo from '../../../Assets/logo.jpeg'
import SectionsList from "../../molecules/SectionsList/SectionsList";
import style from './Navbar.module.css';

const Navbar = () => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <>
        {isMobile ? (
          <Accordion>
            <AccordionSummary
              expandIcon={<Menu sx={{color: "#b3cde0"}}/>}
              sx={{backgroundColor: "#011f4b"}}
            >
              <img className={style.image} src={logo} alt="logo" />
            </AccordionSummary>
            <AccordionDetails sx={{backgroundColor: "#011f4b"}}>
              <SectionsList />
            </AccordionDetails>
          </Accordion>
        ) : (
          <Toolbar className={style.navbar}>
            <Stack alignItems="center" direction="row" gap={4} justifyContent="space-evenly" width="100%">
              <img className={style.imagebig} src={logo} alt="logo" />
              <SectionsList />
            </Stack>
          </Toolbar>
        )};
    </>
  );
}

export default Navbar;