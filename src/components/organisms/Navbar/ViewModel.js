import { useState } from 'react';

const ViewModel = () => {
    const [accordionOpen, setAccordionOpen] = useState(true);
    const handleAccordionState = () => {
        accordionOpen ? setAccordionOpen(false) : setAccordionOpen(true);
    }
    return {accordionOpen, handleAccordionState};
}

export default ViewModel;
