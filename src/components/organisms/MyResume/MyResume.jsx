import { Box } from "@mui/material";

import { ResumeViewer } from "../../molecules";
import style from './MyResume.module.css';

const MyResume = () => {
    return (
        <Box className={style.box}>
            <ResumeViewer />
        </Box>
    )
}

export default MyResume;
