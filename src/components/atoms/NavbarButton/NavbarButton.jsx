import { Button, Stack, Typography } from '@mui/material';

import style from './NavbarButton.module.css';
import ViewModel from './ViewModel';

const NavbarButton = ({ icon, title }) => {

    const { isSelected, setPage } = ViewModel(title);

    return (
        <Button className={isSelected ? style.selected : style.button} disableRipple onClick={() => setPage(title)}>
            <Stack alignItems="center" direction={{sm: "row", md: "column", lg: "row"}} gap={{sm: 4, md: 2}}>
                {icon}
                <Typography>{title}</Typography>
            </Stack>
        </Button>
    );
}

export default NavbarButton;
