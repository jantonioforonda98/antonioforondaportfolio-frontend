import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { getSelectedPage, setSelectedPage } from "../../../redux/reducers";


const ViewModel = (title) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const selected = useSelector(getSelectedPage);

    const isSelected = selected === title;

    const setPage = (path) => {
        const route = path.toLowerCase().replaceAll(' ', '');
        console.log(route);
        navigate(route);
        dispatch(setSelectedPage(path));
    }
    
    return { isSelected, setPage };
}

export default ViewModel;
