import menuReducer, {setSelectedPage, getSelectedPage} from "./menuReducer";

export { menuReducer, setSelectedPage, getSelectedPage };