import { createSlice } from "@reduxjs/toolkit";

const menuSlice = createSlice({
  name: "menu",
  initialState: {
    selectedPage: 'Home',
  },
  reducers: {
    setSelectedPage: (state, action) => {
      state.selectedPage = action.payload;
    },
  },
});

export const getSelectedPage = (state) => state.menuReducer.selectedPage;
export const { setSelectedPage } = menuSlice.actions;

export default menuSlice.reducer;
